#set -g pure_reverse_prompt_symbol_in_vimode false
fish_vi_key_bindings
status --is-interactive; and source (jump shell fish | psub)
#
#source (brew --prefix asdf)/asdf.fish
starship init fish | source

set fish_greeting
set PATH $HOME/.cargo/bin $PATH

source /usr/local/opt/asdf/asdf.fish
