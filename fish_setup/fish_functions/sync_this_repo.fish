function sync_this_repo
  set_color yellow
  echo "Do you want to automatically sync this git repo?"
  set_color red
  echo "Note: You will not be able to manually commit and any changes will be
  automatically captured."
  read -p "echo '(y/n)'" -l answer
  if test "$answer" = "y"
    ~/dotfiles/scripts/git-sync sync
  else
    end
end
