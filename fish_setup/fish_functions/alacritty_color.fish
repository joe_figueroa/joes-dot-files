function alacritty-colorschreme
  set_color black
  set_color -b blue
  echo "ALACRITTY COLORSCHEME Options:"
  set_color normal
  echo "ac-list <-- lists available themes"
  echo "ac-toggle <-- toggle between themes"
  echo ""
end

