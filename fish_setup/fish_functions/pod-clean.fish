function pod-clean
  set_color red
  echo "🧹  CLEANING PODS  🧼"
  set_color blue
  echo 'Cleaning Node Modules'
  set_color normal
  # check if in root dir
  if test -f Podfile.lock
    cd ..
  end
  # reinstall node_modules
  if test -f app.json
    set_color yellow
    echo "Deleting ./node_modules..."
    set_color normal
    rm -rf node_modules yarn.lock package-lock.js
    set_color green
    echo "✅  - Success"
    set_color normal
    set_color blue
    if test $argv
      echo "Re-installing node_modules ($argv)..."
      $argv install
    end
    if not test $argv
      echo "Re-installing node_modules (yarn)..."
      yarn
    end
    set_color normal
    set_color green
    echo "✅  - Node modules re-installed"
    set_color normal
  end
  # (double)check if in ./ios
  if not test -f Podfile.lock
    cd ios
  end
    # reinstall Pods
  set_color yellow
  echo "Deleting Podfile.lock and installed Pods..."
  set_color normal
  rm -rf Podfile.lock Pods/
  set_color green
  echo "✅  - Success"
  set_color normal
  echo \n
  set_color blue
  echo "Re-installing pods..."
  set_color normal
  pod install --repo-update
  set_color blue
  echo "Re-linking..."
  set_color normal
  set_color green
  npx react-native link
  echo "✅  - Finished!"
  if test -f Podfile
    cd -
  end
  set_color -b green
  set_color black
  echo "Go get 'em, 🐯!!!"
  set_color normal
  set_color -b normal
end

