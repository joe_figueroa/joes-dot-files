function rn-start
  set_color black
  set_color -b blue
  echo "Running React Native app..."
  echo "Killing anything running on port 8081 ☠️☠️☠️"
  set_color normal
  echo \n
  set_color red
  kill 8081 
  echo \n
  set_color black
  set_color -b blue
  echo "Starting Metro server..." 
  set_color normal
  echo \n
  npx react-native start --reset-cache --verbose
end

