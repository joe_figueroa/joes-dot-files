# Configuration Files

## Arch System

###  Alacritty

_Modifications:_
- padding (x,y) = 20

#### Installation:
`sudo pacman -S alacritty`
`mkdir ~/.config/alacritty`
`ln -s ~/dotfiles/alacritty.yml ~/.config/alacritty/alacritty.yml`

### Fish

_Modifications:_
- Various functions in `fish_functions/`

#### Installation:
`sudo pacman -S fish`
`cp -a ~/dotfiles/fish_functions/ ~/.config/fish/functions/`

### Tmux

_Modifications:_
- Prefix: `C-w`
- Split window hotkeys
- Enable mouse input
- Statusline mods

#### Installation:
`sudo pacman -S tmux`
`ln -s ~/dotfiles/tmux.conf ~/.tmux.conf`

### Vim/Neovim
Mostly use Neovim--some plugins may not work with Vim

_Modifications:_
- See `vimrc`

#### Installation:
Install plugin manager: [Vim Plug](https://github.com/junegunn/vim-plug)
Vim: `ln -s ~/dotfiles/vimrc ~/.vimrc`

Neovim:
`sudo pacman -S neovim` [see Neovim git](https://github.com/neovim/neovim/wiki/Installing-Neovim)
`mkdir ~/.config/neovim`
`ln -s ~/dotfiles/vimrc ~/.config/nvim/init.vim`
####  Syntax for .ini files
mkdir -p ~/.config/nvim/after/sytax/
ln -s ~/dotfiles/dosini.vim ./ 

### NVM and NODE
Install NVM from [AUR](https://aur.archlinux.org/packages/nvm/)
Install [Fisher](https://github.com/jorgebucaran/fisher)
Use Fisher to install [nvm.fish](https://github.com/jorgebucaran/nvm.fish)
`nvm use lts`

## WSL

Same process but using Ubuntu's `sudo apt install...`


